
/**
  *
  * @author Falk Wallis
  *
  */

package de.wallisfalk.randomgenerators

import org.scalatest.FlatSpec
import de.wallisfalk.randomgenerators.{PersistableConstantRandomGenerator => PersistableGenerator}


class PersistableConstantRandomGeneratorFlatSpec extends FlatSpec {
  "A 'PersistableConstantRandomIntGenerator'" should "generate the same sequence of random integers for a randomly given set of seed, range and seedEveryProcess'" in {
    (1 until 100).foreach(_ => {
      val seed = scala.util.Random.nextInt(100)
      val seedEveryProcesses = scala.util.Random.nextInt(10) + 2
      val zeroToNumberExclusive = scala.util.Random.nextInt(100) + 1
      val generator1 = PersistableGenerator(seed, seedEveryProcesses)
      var generator1AndInteger = PersistableGenerator.nextInteger(generator1, zeroToNumberExclusive)
      val testSequenceLength = 50
      val result1 = (1 until testSequenceLength).map(_ => {
        generator1AndInteger = PersistableGenerator.nextInteger(generator1AndInteger._1, zeroToNumberExclusive)
        generator1AndInteger._2
      })

      val generator2 = PersistableGenerator(seed, seedEveryProcesses)
      var generator2AndInteger = PersistableGenerator.nextInteger(generator2, zeroToNumberExclusive)
      val result2 = (1 until testSequenceLength).map(_ => {
        generator2AndInteger = PersistableGenerator.nextInteger(generator2AndInteger._1, zeroToNumberExclusive)
        generator2AndInteger._2
      })
      assert(result1 == result2)
    })
  }
}
