
/**
  *
  * @author Falk Wallis
  *
  */

package de.wallisfalk.randomgenerators

import org.scalatest.{Matchers, PropSpec}
import de.wallisfalk.randomgenerators.{PersistableConstantRandomGenerator => PersistableGenerator}
import org.scalatest.prop.TableDrivenPropertyChecks


class PersistableConstantRandomGeneratorPropSpec extends PropSpec  with TableDrivenPropertyChecks with Matchers {

   def testOneSet(seed: Int, seedEveryProcesses: Int, range: Int, testSequenceLength: Int, saveAtNum: Int) : Unit = {
      println("")
      println(s"Test with saveAtNum: $saveAtNum")

      val generator1 = PersistableGenerator(seed, seedEveryProcesses)
      var generator1AndInteger = PersistableGenerator.nextInteger(generator1, range)
      val resultFirst = generator1AndInteger._2
      val result1 = ((resultFirst + "-" + generator1AndInteger._1.numberGenerations) +: (1 until testSequenceLength).map(_ => {
         generator1AndInteger = PersistableGenerator.nextInteger(generator1AndInteger._1, range)
         generator1AndInteger._2 + "-" + generator1AndInteger._1.numberGenerations
      })).toList

      if (saveAtNum == 0) {
         val generator2 = PersistableGenerator(seed, seedEveryProcesses)
         // extract parameters and save it ...
         val parameters = generator2.extractParameters
         // load and restore generator ...
         val generator3 = PersistableGenerator.restore(parameters)
         var generator3AndInteger = PersistableGenerator.nextInteger(generator3, range)
         val result3First = generator3AndInteger._2
         val result3 = ((result3First + "-" + generator3AndInteger._1.numberGenerations) +: (1 until testSequenceLength).map(_ => {
            generator3AndInteger = PersistableGenerator.nextInteger(generator3AndInteger._1, range)
            generator3AndInteger._2 + "-" + generator3AndInteger._1.numberGenerations
         })).toList
         println("Test result1: " + result1)
         println("Test result3: " + result3)
         assert(result1 == result3)
      }
      else {
         val generator2 = PersistableGenerator(seed, seedEveryProcesses)
         var generator2AndInteger = PersistableGenerator.nextInteger(generator2, range)
         val result2First = generator2AndInteger._2
         val result2 = ((result2First + "-" + generator2AndInteger._1.numberGenerations) +: (1 until saveAtNum).map(_ => {
            generator2AndInteger = PersistableGenerator.nextInteger(generator2AndInteger._1, range)
            generator2AndInteger._2 + "-" + generator2AndInteger._1.numberGenerations
         })).toList
         // extract parameters and save it ...
         val parameters = generator2AndInteger._1.extractParameters
         // load and restore generator ...
         val generator3 = PersistableGenerator.restore(parameters)
         var generator3AndInteger = PersistableGenerator.nextInteger(generator3, range)
         val result3First = generator3AndInteger._2
         var result3 = result2 ::: List(result3First + "-" + generator3AndInteger._1.numberGenerations)
         result3 = result3 ::: (saveAtNum + 1 until testSequenceLength).map(_ => {
            generator3AndInteger = PersistableGenerator.nextInteger(generator3AndInteger._1, range)
            generator3AndInteger._2 + "-" + generator3AndInteger._1.numberGenerations
         }).toList

         println("Test result1: " + result1)
         println("Test result3: " + result3)

         assert(result1 == result3)
      }
   }

   val saveAtNums =
      Table(
         "saveAtNum",
         0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29
      )

   property("An interrupted and saved generator should proceed well") {
      forAll(saveAtNums) { saveAtNum =>
         testOneSet(100, 10, 100, 30, saveAtNum)
      }
   }

}
