
/**
  *
  * @author Falk Wallis
  *
  */

package de.wallisfalk.randomgenerators

import scala.util.Random


/**
  * Case class 'RestoreParameters' used within methods 'constantRandomGenerator.extractParameters' and
  * 'PersistableConstantRandomGenerator.restore' for saving purposes.
  *
  */
case class RestoreParameters(freshRandom: Boolean, lastSeed: Long, seedEveryProcesses: Int, numberGenerations: Int) {
  override def toString: String = s"RestoreParameters(freshRandom='$freshRandom', lastSeed='$lastSeed', seedEveryProcess='$seedEveryProcesses', numberGenerations='$numberGenerations')"
}

/**
  * Sealed trait for a generalization of all generators.
  *
  */
sealed trait RandomGenerator

/**
  * Case class 'PersistableConstantRandomGenerator' and its companion object. This generator is designed for having
  * the ability to generate a predefined sequence of random outputs. The second ability is to save a state of the
  * generator and to restore it, to proceed the random sequence.
  *
  */
case class PersistableConstantRandomGenerator private(seed: Long, seedEveryNumberGenerations: Int, randomOption: Option[Random], numberGenerations: Int) extends RandomGenerator {

  def extractParameters: RestoreParameters = RestoreParameters(freshRandom.get, this.seed, seedEveryNumberGenerations, numberGenerations)

  private def nextRandomOutput[T](f: () => T) : (PersistableConstantRandomGenerator, T) = {
    if (freshRandom.contains(true)) {
      freshRandom = Some(false)
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), f.apply())
    }
    else if (numberGenerations == seedEveryNumberGenerations - 1)
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), f.apply())
    else if (numberGenerations == seedEveryNumberGenerations)
      (PersistableConstantRandomGenerator(seed + 1, seedEveryNumberGenerations, None, 0), 0)._1.nextRandomOutput(f)
    else
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), f.apply())
  }

  private def nextIntegerPrivate(range: Int) : (PersistableConstantRandomGenerator, Int) = {
    require(range > 0, "The method 'persistableConstantRandomGenerator.nextInteger's parameter 'range' must be greater than zero!")

    if (freshRandom.contains(true)) {
      freshRandom = Some(false)
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), random.nextInt(range))
    }
    else if (numberGenerations == seedEveryNumberGenerations - 1)
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), random.nextInt(range))
    else if (numberGenerations == seedEveryNumberGenerations)
      (PersistableConstantRandomGenerator(seed + 1, seedEveryNumberGenerations, None, 0), 0)._1.nextIntegerPrivate(range)
    else
      (PersistableConstantRandomGenerator(seed, seedEveryNumberGenerations, Some(random), this.numberGenerations + 1), random.nextInt(range))
  }
  require(seedEveryNumberGenerations >= 2, "The constructor 'PersistableConstantRandomGenerator's parameter 'seedEveryNumberGenerations' must be equal or greater than two!")

  var freshRandom : Option[Boolean] = None
  val random: Random = if (randomOption.isEmpty) {
    freshRandom = Some(true)
    val r = new Random
    r.setSeed(seed)
    r
  }
  else {
    freshRandom = Some(false)
    randomOption.get
  }

  private def getRandom : Random = random

  override def toString: String = s"PersistableConstantRandomGenerator(seed='$seed', seedEveryNumberGenerations='$seedEveryNumberGenerations', numberGenerations='$numberGenerations')"
}


/**
  * Case object 'PersistableConstantRandomGenerator' with its call to the private constructor and the different methods
  * for 'nextProcessStep' and 'restore'. The corresponding method to 'restore' is the instance method 'extractParameters'.
  *
  */
case object PersistableConstantRandomGenerator {

  /**
    * The apply method for calling the private constructor.
    *
    * @param seed The seed of the internal scala.util.Random instance of type Long
    * @param seedEveryProcesses The number of generations when the internal scala.util.Random instance get a fresh
    *                           instance with a new seed (old seed incremented by one), it represents the maximum
    *                           number generations internally made and thrown away to get to the previous saved state.
    *
    */
  def apply(seed: Long, seedEveryProcesses: Int): PersistableConstantRandomGenerator =
    PersistableConstantRandomGenerator(seed, seedEveryProcesses, None, 0)


  def restore(restoreParameters: RestoreParameters): PersistableConstantRandomGenerator = {
    val generator = apply(restoreParameters.lastSeed, restoreParameters.seedEveryProcesses, None, 0)
    var generatorAndInteger = (generator, 0)
    if (restoreParameters.freshRandom)
      generator
    else {
      Range(1, restoreParameters.numberGenerations + 1).foreach(_ => {
        generatorAndInteger = nextInteger(generatorAndInteger._1)
      })
      generatorAndInteger._1
    }
  }

  def nextInteger(persistableGenerator: PersistableConstantRandomGenerator, range : Int = Int.MaxValue) :  (PersistableConstantRandomGenerator, Int) =
    persistableGenerator.nextIntegerPrivate(range)

  def nextLong(persistableGenerator: PersistableConstantRandomGenerator) : (PersistableConstantRandomGenerator, Long) =
    persistableGenerator.nextRandomOutput({ () => persistableGenerator.random.nextLong() })
}
